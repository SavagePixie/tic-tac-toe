package game

import cats.effect.IO
import cats.effect.IOApp
import cats.effect.ExitCode
import scala.io.Source
import scala.io.StdIn

object Main extends IOApp {
	implicit class IntOps(x: Int) {
		def between(lower: Int, upper: Int): Boolean = x >= lower && x <= upper
	}

	type Board = Vector[Vector[Cell]]
	type CellIndex = (Int, Int)

	trait Cell {
		def show: String = this match {
			case Empty => " "
			case X => "X"
			case O => "O"
		}

		def toPlayer: Option[Player] = this match {
			case Empty => None
			case O => Some(Player.O)
			case X => Some(Player.X)
		}
	}

	object CellIndex {
		def fromString(value: String): Either[String, CellIndex] = {
			value.split("").map(_.toIntOption) match {
				case Array(Some(a), Some(b)) if a.between(1, 3) && b.between(1, 3) => Right((a, b))
				case Array(Some(_), Some(_)) => Left("Move out of bounds, try again.")
				case _ => Left("Yo, try with a number?")
			}
		}
	}

	case object Empty extends Cell
	case object O extends Cell
	case object X extends Cell

	trait Player {
		def other: Player = {
			this match {
				case Player.O => Player.X
				case Player.X => Player.O
			}
		}

		def toCell: Cell = {
			this match {
				case Player.O => O
				case Player.X => X
			}
		}
	}
	object Player {
		case object O extends Player
		case object X extends Player
	}

	trait GameState {
		def message: String
	}
	object GameState {
		case object Draw extends GameState {
			def message: String = "'Tis a draw!"
		}
		case object Ongoing extends GameState {
			def message: String = "Game's still a-goin'!"
		}
		case class Winner(player: Player) extends GameState {
			def message: String = s"$player wins!"
		}
	}

	case class Move(player: Player, x: Int, y: Int) {
		lazy val getY: Int = y - 1
		lazy val getX: Int = x - 1
	}

	def nextGameState(board: Board): GameState = {
		rowWinner(board)
			.orElse(colWinner(board))
			.orElse(diagonWinner(board))
			.map(GameState.Winner)
			.getOrElse(isBoardFull(board))
	}

	def colWinner(board: Board): Option[Player] = {
		rowWinner(board.transpose)
	}

	def diagonWinner(board: Board): Option[Player] = {
		rowWinner(Vector(
			Vector(board(0)(0), board(1)(1), board(2)(2)),
			Vector(board(0)(2), board(1)(1), board(2)(0))
		))
	}

	def rowWinner(board: Board): Option[Player] = {
		board
			.find(row => row.forall(cell => cell != Empty && cell == row.head))
			.flatMap(_.head.toPlayer)
	}

	def isBoardFull(board: Board): GameState = {
		if (board.forall(_.forall(_ != Empty))) GameState.Draw
		else GameState.Ongoing
	}

	def nextTurn(board: Board, player: Player): IO[(Board, Player)] = {
		printBoard(board)
			.flatMap(_ => readMove(board, player))
			.map(runMove(board))
			.flatMap { tuple =>
				val (board, player) = tuple
				nextGameState(board) match {
					case GameState.Ongoing => nextTurn(board, player)
					case state => printBoard(board) >> IO(println(state.message)) >> playAgain
				}
			}
	}

	def playAgain: IO[(Board, Player)] = {
		IO(StdIn.readLine("Play again? [Y/n] "))
			.flatMap {
				case "n" | "N" => IO((EmptyBoard, Player.X))
				case _ => nextTurn(EmptyBoard, Player.X)
			}
	}

	def printBoard(board: Board): IO[Unit] = IO {
		println(board.map(_.map(_.show).mkString(" ", " | ", "\n")).mkString("-" * 10 ++ "\n"))
	}

	def readMove(board: Board, player: Player): IO[Move] = {
		IO(StdIn.readLine(s"Player $player's turn: "))
			.map(CellIndex.fromString)
			.map(_.map { case (x, y) => Move(player, x, y)})
			.flatMap {
				case Right(move) if validateMove(board, move) => IO(move)
				case Right(_) => IO(println("Invalid move, try again")) >> readMove(board, player)
				case Left(reason) => IO(println(reason)) >> readMove(board, player)
			}
	}

	def runMove(board: Board) (move: Move): (Board, Player) = {
		val row = board(move.getY)
			.updated(move.getX, move.player.toCell)
		val newBoard = board.updated(move.getY, row)
		(newBoard, move.player.other)
	}

	def validateMove(board: Board, move: Move): Boolean = {
		board(move.getY)(move.getX) == Empty
	}

	val EmptyBoard = Vector.fill(3)(Vector.fill(3)(Empty))

	override def run(args: List[String]): IO[ExitCode] = {
		nextTurn(EmptyBoard, Player.X).as(ExitCode.Success)
	}
}
