import sbt._

object Dependencies {
	lazy val munit = "org.scalameta" %% "munit" % "0.7.29"
	lazy val cats = Seq(
		"org.typelevel" %% "cats-core" % "2.9.0",
		"org.typelevel" %% "cats-effect" % "3.5.1"
	)
}
